#!/bin/bash

# OS updates ###################################################################
apt-get update && apt-get upgrade -y --force-yes

# Download Scripts #############################################################

RepoLocation=https://gitlab.com/mkolakowski/minecraft-linux-scripts/raw/master
ScriptLocation=/media/minecraft/scripts

wget $RepoLocation/backup.sh -P $ScriptLocation
chmod u+x $ScriptLocation/backup.sh

wget $RepoLocation/connect.sh -P $ScriptLocation
chmod u+x $ScriptLocation/connect.sh

wget $RepoLocation/restart.sh -P $ScriptLocation
chmod u+x $ScriptLocation/restart.sh

wget $RepoLocation/restore.sh -P $ScriptLocation
chmod u+x $ScriptLocation/restore.sh

wget $RepoLocation/shutdown.sh -P $ScriptLocation
chmod u+x $ScriptLocation/shutdown.sh

wget $RepoLocation/starter.sh -P $ScriptLocation
chmod u+x $ScriptLocation/starter.sh

wget $RepoLocation/update.sh -P $ScriptLocation
chmod u+x $ScriptLocation/update.sh