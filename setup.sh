#!/bin/bash
# ------------------------------------------------------------------------------
# - Minecraft Server setup
# - Created by Matthew Kolakowski
# - Updated on 2019.06.10
# -
# - This script will install the dependancys for the Minecraft server as well
# - as the dependancys for the backup and starter scripts
#
# - Built for Ubuntu 18.04
#-------------------------------------------------------------------------------

# OS updates ###################################################################
apt-get update && apt-get upgrade -y --force-yes

# Install Time #################################################################

# Needed for backup script -----------------------------------------------------
sudo apt install -y zip

# File Management --------------------------------------------------------------
sudo apt install -y mc

# File Copy/Backup -------------------------------------------------------------
curl https://rclone.org/install.sh | sudo bash

# Disk Management --------------------------------------------------------------
sudo apt install -y gparted

# Needed for Restore Scripts ---------------------------------------------------
sudo apt install -y unzip

# Needed for all scripts -------------------------------------------------------
sudo apt install -y screen

# Needed to run server ---------------------------------------------------------
sudo apt install -y openjdk-11-jre-headless

# Secutity ---------------------------------------------------------------------
apt install -y fail2ban
apt install -y sendmail-bin sendmail

# Keeps the Scripts up to date -------------------------------------------------
sudo apt install -y git

# Disk Util --------------------------------------------------------------------
sudo apt install -y parted

# Disk Setup ###################################################################

# Mount sdb1 here
mkdir /media/minecraft

# creating Minecraft Partitions
parted -a optimal /dev/sdb mkpart primary 0% 4096MB


# Download Scripts #############################################################

RepoLocation=https://gitlab.com/mkolakowski/minecraft-linux-scripts/raw/master
ScriptLocation=/media/minecraft/scripts

wget $RepoLocation/backup.sh -P $ScriptLocation
chmod u+x $ScriptLocation/backup.sh

wget $RepoLocation/connect.sh -P $ScriptLocation
chmod u+x $ScriptLocation/connect.sh

wget $RepoLocation/restart.sh -P $ScriptLocation
chmod u+x $ScriptLocation/restart.sh

wget $RepoLocation/restore.sh -P $ScriptLocation
chmod u+x $ScriptLocation/restore.sh

wget $RepoLocation/shutdown.sh -P $ScriptLocation
chmod u+x $ScriptLocation/shutdown.sh

wget $RepoLocation/starter.sh -P $ScriptLocation
chmod u+x $ScriptLocation/starter.sh

# Minecraft Setup ##############################################################



# Security Setup ###############################################################

# Firewall - Drop Connection Rule ----------------------------------------------

# Variables
FirewallSecs='3'
FirewallHitCount='15'

# Firewall - Drop Connection Rule 
sudo iptables -I INPUT -p tcp --dport 25565 -m state --state NEW -m recent --set
sudo iptables -I INPUT -p tcp --dport 25565 -m state --state NEW -m recent --update --seconds $FirewallSecs --hitcount $FirewallHitCount -j DROP

# What this basically does is, if someone tries to connect to your server 
# $FirewallHitCount times from the same IP simultaneously within $FirewallSecs 
# seconds, it'll drop the new connections until the old ones are cleared up


# Fail2Ban Setup - RESERVED ----------------------------------------------------

# Configure Fail2ban for SSH

# Configure Fail2ban for Minecraft


# Start Fail2Ban
fail2ban-client start

# Print Status
fail2ban-client status