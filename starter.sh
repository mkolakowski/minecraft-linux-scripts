#!/bin/bash

# Sets Timexone ----------------------------------------------------------------
     TZ='America/New_York'; export TZ

# Start server -----------------------------------------------------------------
     screen -d -m -S mcs java -Xms1G -Xmx5G -jar ./server.jar
     screen -r mcs; bash
