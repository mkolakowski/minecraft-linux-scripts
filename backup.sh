#!/bin/bash
echo ---------------------------------------------------------------------------
echo - Minecraft Server Backup
echo - Created by Matthew Kolakowski
echo - Updated on 2019.04.04.1030
echo -
echo - This script will Set the correct timezone, Pause auto saving on the 
echo - minecraft server and perform a full save. The Server Folder will  be 
echo - zipped up and uploaded to two locations as spesifed below, and will turn 
echo - on auto saving on the minecraft server and will delete the Zip file
echo ---------------------------------------------------------------------------
echo
echo
echo ---------------------------------------------------------------------------
echo Variables
echo ---------------------------------------------------------------------------
     # Start time of the script
     starttime=$(date +%Y%m%d-%H%M%S)
     
     # $SN       Servers name
     SN='kolakraft-1.14.2'
     
     # $ SD     Server Directory
     SD=/media/minecraft/kolakraft
     
     # $BL Backup Location
     BL=/media/backup/$SN
     
     # $BD      Back Directory
     BD=$BL/backup-$SN--$starttime.zip
     
     # FTPBackup    Location of FTP Backups - can be changed to any location
     FTPBackup=backup:/mnt/user/Backup-MC/$SN
     
     # CloudBack    Location of Cloud Backups - can be changed to any location
     CloudBack=remote:/minecraft/$SN
echo
echo
echo ---------------------------------------------------------------------------
echo making folders
echo ---------------------------------------------------------------------------
     mkdir $SD/crontab
     mkdir $SD/logs-latest/
     mkdir $SD/logs-FTP
     mkdir $SD/logs-cloud 
     mkdir $BL
echo
echo
echo ---------------------------------------------------------------------------
echo Setting Time Zone
echo ---------------------------------------------------------------------------
     TZ='America/New_York'; export TZ
echo
echo
echo ---------------------------------------------------------------------------
echo Backup Crontab to minecraft directory
echo ---------------------------------------------------------------------------
     crontab -l > $SD/crontab/crontab--$starttime.txt
echo
echo
echo ---------------------------------------------------------------------------
echo Turn off Auto-Save then Save Game
echo ---------------------------------------------------------------------------
     screen -r -X stuff 'say Starting Backup\n'
     screen -r -X stuff 'say save-off\n'
     screen -r -X stuff 'say save-all\n'
echo
echo
echo ---------------------------------------------------------------------------
echo Zip Minecraft Folder
echo ---------------------------------------------------------------------------
     
     zip -r $BD $SD -x $BD --exclude="*/.*"
echo
echo
echo ---------------------------------------------------------------------------
echo Copying Live Server Log
echo ---------------------------------------------------------------------------
     cp $SD/logs/latest.log $SD/logs-latest/$SN-latest-$starttime.log
echo
echo
echo ---------------------------------------------------------------------------
echo Turn on Auto-Save
echo ---------------------------------------------------------------------------
     screen -r -X stuff 'say Backup Complete\n'
     screen -r -X stuff 'say save-on\n'
echo
echo
echo ---------------------------------------------------------------------------
echo Copy Backup to Network Share via FTP
echo ---------------------------------------------------------------------------
     rclone copy -P $BD $FTPBackup/Backup/$(date +%Y-%m-%d) --log-file=$SD/logs-FTP/$SN-FTP-$starttime.log
echo
echo
echo ---------------------------------------------------------------------------
echo Upload Backup to GDrive
echo ---------------------------------------------------------------------------
     rclone copy -P $BD $CloudBack/Backup/$(date +%Y-%m-%d) --log-file=$SD/logs-cloud/$SN-cloud-$starttime.log
echo
echo
echo ---------------------------------------------------------------------------
echo Remove Backup Zips older than 7 days
echo ---------------------------------------------------------------------------
     find $BL -type f -name "*.zip" -mtime +14 -exec rm {} \;
echo
echo
echo ---------------------------------------------------------------------------
echo backup crontab to cloud and FTP
echo ---------------------------------------------------------------------------
     rclone copy -P $SD/crontab $CloudBack/Crontab
     rclone copy -P $SD/crontab $FTPBackup/Crontab
echo
echo
echo ---------------------------------------------------------------------------
echo Backup Server Logs to FTP
echo ---------------------------------------------------------------------------
     rclone copy -P $SD/logs $FTPBackup/Logs-Server
     rclone copy -P $SD/logs-FTP $FTPBackup/Logs-FTP
     rclone copy -P $SD/logs-cloud $FTPBackup/Logs-Cloud
     rclone copy -P $SD/logs-latest $FTPBackup/Logs-Latest
     rclone copy -P $SD/crash-reports $FTPBackup/Crash-Reports
echo
echo
echo ---------------------------------------------------------------------------
echo Backup Server Logs to Cloud
echo ---------------------------------------------------------------------------
     rclone copy -P $SD/logs $CloudBack/Logs-Server
     rclone copy -P $SD/logs-FTP $CloudBack/Logs-FTP
     rclone copy -P $SD/logs-cloud $CloudBack/Logs-Cloud
     rclone copy -P $SD/logs-latest $CloudBack/Logs-Latest
     rclone copy -P $SD/crash-reports  $CloudBack/Crash-Reports
echo
echo
echo ---------------------------------------------------------------------------
echo Deleting Copy of Latest log file
echo ---------------------------------------------------------------------------
     rm $SD/latest-$starttime.log
echo
echo
echo ---------------------------------------------------------------------------
echo Removing logs older than 14 Days
echo ---------------------------------------------------------------------------
     find $SD/logs -type f -name "*.gz" -mtime +14 -exec rm {} \;
     find $SD/logs-FTP -type f -name "*.gz" -mtime +14 -exec rm {} \;
     find $SD/logs-cloud -type f -name "*.gz" -mtime +14 -exec rm {} \;
     find $SD/logs-latest -type f -name "*.gz" -mtime +14 -exec rm {} \;
     find $SD/crash-reports -type f -name "*.gz" -mtime +14 -exec rm {} \;
echo
echo
echo ---------------------------------------------------------------------------
echo Backup Scripts to FTP
echo ---------------------------------------------------------------------------
     rclone copy -P $SD/scripts $FTPBackup/Scripts/$(date +%Y-%m-%d)/Scripts-$starttime
echo
echo
echo ---------------------------------------------------------------------------
echo Backup Scripts to Cloud
echo ---------------------------------------------------------------------------
     rclone copy -P $SD/scripts $CloudBack/Scripts/$(date +%Y-%m-%d)/Scripts-$starttime
echo
echo
echo ---------------------------------------------------------------------------
echo Chcking system disk space
echo ---------------------------------------------------------------------------
     df -H /dev/sdb1
echo     
     df -H /dev/sdc1
echo
echo
echo -----------------------------------------------------
echo Job Started:	$starttime
echo Job Ended:		$(date +%Y%m%d-%H%M%S)
echo -----------------------------------------------------