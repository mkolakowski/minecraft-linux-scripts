#!/bin/bash

echo -------------------------------------------------------------------------------------
echo Stop server
echo -------------------------------------------------------------------------------------
     screen -r -X stuff 'say ----------------------------------\n'
     screen -r -X stuff 'say Shutting down server in 60 seconds\n'
     sleep 60
     screen -r -X stuff 'stop\n'