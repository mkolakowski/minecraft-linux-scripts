#!/bin/bash
echo -------------------------------------------------------------------------------------
echo - Minecraft Server Restarter
echo - Created by Matthew Kolakowski
echo - Updated on 2018.11.05.2148
echo - 
echo - This script will perform a backup on the server and restart the client
echo -------------------------------------------------------------------------------------
echo
echo
echo -------------------------------------------------------------------------------------
echo Restart server
echo -------------------------------------------------------------------------------------
     screen -r -X stuff 'say Starting Server Restart in 60 seconds\n'
     echo Putting MC Server to sleep 60 Seconds
#     sleep 60
     screen -r -X stuff 'stop'

#-- Backup
     ./mc-backup.sh
#
#-- Starter
     ./mc-starter.sh